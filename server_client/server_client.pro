
#QT += qt core gui

CONFIG += qt gui
TEMPLATE = app
CONFIG += app_bundle
#QT += qt
QT += widgets

SOURCES += main.cpp \
    network.cpp \
    mainwindow.cpp \
    protocol.cpp \
    dialog_password.cpp \
    store_data.cpp

HEADERS += \
    network.h \
    inclusions.h \
    mainwindow.h \
    protocol.h \
    dialog_password.h \
    store_data.h

FORMS += \
    mainwindow.ui \
    dialog_password.ui


QMAKE_CXXFLAGS += "-std=c++11"
CXXFLAGS="-std=c++0x"


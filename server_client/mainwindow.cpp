#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "inclusions.h"
#include "protocol.h"

#define CONNECTION_IS "online"
#define CONNECTION_MISSING "offline"

#define SHOW_MESSAGE(s) { \
    QMessageBox message_box;  \
    message_box.setText(s);  \
    message_box.setWindowTitle("Notification");   \
    message_box.show();  \
    message_box.exec();  \
}


MainWindow::MainWindow(QWidget *parent) :  QMainWindow(parent),   ui(new Ui::MainWindow)
{
//   connect_disconnect_counter  = 0;
    _network = new Client_network;
    ui->setupUi(this);
    _timer = new QTimer(this);
    _timer->setInterval(CHECKING_INTERVAL_SERVER_PRESENCE);


    ui->textEdit->setReadOnly(true);
    connect(_timer,SIGNAL(timeout()),this,SLOT(connection_checker()));


//    if ( _network->is_server_was_found())
//     ui->label->setText(CONNECTION_IS);
//    else
     ui->label->setText(CONNECTION_MISSING);
}

void MainWindow::on_pushButton_3_clicked() // connect
{
//    _timer->start();

//    if( _network-)
//    if ( connect_disconnect_counter ==  CONNECTED_VALUE) {
//        SHOW_MESSAGE("already connected");
//        return;
//    } else
//        connect_disconnect_counter = CONNECTED_VALUE;

    if ( _network->is_connected() ) {
        SHOW_MESSAGE("already connected");
        return; // we are already connected
    }

    if (  _network->initial_try_to_connect() == true ) {
        ui->label->setText(CONNECTION_IS);
        _timer->start();
    }
}

void MainWindow::on_pushButton_4_clicked() // disconnect
{
    _timer->stop();
    ui->label->setText(CONNECTION_MISSING);
    _network->disconnect();
//    if ( connect_disconnect_counter  == DISCONNECTED_VALUE ) {
//        SHOW_MESSAGE("already disconnected");
//        return;
//    } else
//        connect_disconnect_counter = DISCONNECTED_VALUE;

//    ui->label->setText(CONNECTION_MISSING);
//    _timer->stop();
//    _network->disconnect();
}


void MainWindow::connection_checker() {
    cout<<"connection is being checked\n";
    if ( _network->is_server_was_found()) {
     ui->label->setText(CONNECTION_IS);
     _network->set_connected_state();
    }
    else {
     ui->label->setText(CONNECTION_MISSING);
     _network->set_disconnected_state();
    }
}


MainWindow::~MainWindow() {
    _timer->stop();
    delete _timer;
    if (_network != NULL) delete _network;
    delete ui;
}



void MainWindow::on_pushButton_pressed() {

    if ( _network->is_server_was_found() == false) {
        QMessageBox dd;
        dd.setWindowModality(Qt::ApplicationModal);

        dd.setVisible(true);
        dd.setText("offline, message can't be sent");
        dd.exec();
        return;
    }

    string out;
    QString text = ui->plainTextEdit->toPlainText();
//    Protocol::basic_handshake_pack()

    QTime time = QTime::currentTime();
    QString line_time = time.toString("hh:mm:ss") + " ";
    line_time += text;

//    Protocol::basic_handshake_pack()

    _network->Nsend(line_time.toStdString().data(),line_time.length());

    ui->textEdit->append(line_time);
    ui->plainTextEdit->clear();
}



void MainWindow::on_pushButton_2_clicked() {   // authorisation button

         Dialog_password dial;
         dial.set_form_data(Store_data::read());
         dial.show();
         dial.exec();

         Store_data::write( dial.get_data_from_form());
}

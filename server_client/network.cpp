#include "network.h"
#include "protocol.h"
#include "dialog_password.h"


Client_network::Client_network() { 
//  try_to_connect();
}

Authenticate Client_network::get_name_passw_from_store() {
    return Store_data::read();
}

int Client_network::Ninitialise() {

    string out;
    if ( Protocol::basic_pack(Store_data::read(),out,AUTHORISATION)  == false )
        REPORT_CONTINUE("error packing");

    cout<<out<<"\n";

    Authenticate myauth;
    if( Protocol::basic_unpack(out,myauth,AUTHORISATION) == false )
        REPORT_CONTINUE("error unpacking");

    if (  this->Nsend(out) != EXIT_SUCCESS ) {
        REPORT_CONTINUE("server is lost");
       return false;
    }

  cout<<endl;
    cout<<"\n";
    cout<<myauth._name<<"  "<<myauth._password<<"\n";

    char *pack= this->Nreceive(_socket_fd);
    if (Protocol::basic_check_presense_headers(pack,USUAL) == false )
        REPORT_CONTINUE("error unpacking");


    if (  Protocol::usual_unpack(pack,out) == false )
        REPORT_CONTINUE("error usual unpacking");


    cout<<"result of initialisation \n"<<out<<endl;

    if( out == "success") {
        cout<<"passed\n";
        this->set_connected_state();
       return true; // passed initialisation
    }
  return false;

}

int Client_network::initial_try_to_connect() {

    _server_is_present = false;
    struct addrinfo hints;
    struct addrinfo  *result=NULL, *result_pointer = NULL;
    memset(&hints,0,sizeof(struct addrinfo ));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;


    int error =  getaddrinfo(ADDRESS,PORT,&hints,&result);

    if ( error != 0) {
        cerr<<"getaddrinfo error  "<<__FILE__<<" "<<__LINE__<<"  "<<gai_strerror(error)<<"  \n";
        _server_is_present = false;
//        exit(EXIT_FAILURE);
    }

    for( result_pointer = result;  result_pointer != NULL; result_pointer = result_pointer->ai_next) {

        _socket_fd = socket(result_pointer->ai_family,
                     result_pointer->ai_socktype,
                     result_pointer->ai_protocol);
        if ( _socket_fd == -1)
            continue;

        if ( connect(_socket_fd,result_pointer->ai_addr,
                     result_pointer->ai_addrlen) != -1)
            break; // successfully  connected

        close(_socket_fd);
    }

   freeaddrinfo(result);


    if( result_pointer == NULL) // no one address fitted
    {
        cerr<<"server isn't found, could not connect\n";
        _server_is_present = false;
        return false;
//        exit(EXIT_FAILURE);
    }
    else
   {
     return this->Ninitialise(); // everything goes according to plan
    }
}

void Client_network::set_connected_state() {
     _server_is_present = true;
}

void Client_network::set_disconnected_state() {
    _server_is_present = false;
}

int Client_network::is_connected() const {
    return _server_is_present;
}

int Client_network::is_server_was_found() {
//    if ( _server_is_present == false) {  // let us try to find it
//      initial_try_to_connect();
//    }
//    else { // check if connection is still accessible
       _server_is_present =  this->send_test_presense_data();
//    }

   return _server_is_present;
}


int Client_network::send_test_presense_data() {
    string data = "checking state";

    if ( this->Nsend(data.c_str(),1) == EXIT_SUCCESS ) {
       return true;
    }
    else {
      cout<<"sending test presense data failed\n";
      return false;
    }
}

void Client_network::disconnect() {
  _server_is_present = false;
  close(_socket_fd);
//  shutdown(_socket_fd,SHUT_RDWR);

}


int Client_network::CN_handshake() {
}

char *Client_network::Nreceive(int socket) {
    static char buf[MAX_SIZE_BUF]={0, };
    int error =  recv(socket,buf,MAX_SIZE_BUF,0);
    cout<<buf<<"\n";
    return buf;
}

int Client_network::Nsend(const char *data, int size) {

    int error = 0;
    if( (error = send(_socket_fd,data,size,0) )!= size ) {

        if ( error == -1) {
            cerr<< strerror(errno) << " erorr write\n";
            return error_send;
        } else  {
            cerr<<"asked "<<size<<"sent "<< error <<"too big chunk\n";
            return error_send;
        }
    }

    return EXIT_SUCCESS;
}

int Client_network::Nsend(const string &data) {
    int error = 0;
    if( (error = send(_socket_fd,data.c_str(),data.size(),0) )!= data.size() ) {

        if ( error == -1) {
            cerr<< strerror(errno) << " erorr write\n";
            return error_send;
        } else  {
            cerr<<"asked "<<data.size()<<"sent "<< error <<"too big chunk\n";
            return error_send;
        }
    }
    return EXIT_SUCCESS;
}
    //   char *data = "123456";
//       char data[100]={1,};
//       char *accept = gl_data;


//       for( int  i =0; i < 2; i ++) {

//        cout<<"bytes read  "<<data_exchange(sfd,data,accept,1000)<<"\n";
//        cout<<"first read\n";
//    //    sleep(1);
//       cout<<accept<<"\n";
//       sleep(2);

//      }

//       cout<<"finished successful\n";
//       shutdown(sfd,SHUT_RDWR);
    //   sleep(1);

/*
int Client_network::data_exchange(int socket, const char *data_inp, char *data_out, int size) {

    int error = 0;
    if( (error = send(socket,data_inp,size,0) )!= size ) {

        if ( error == -1) {
            cerr<< strerror(errno) << " erorr write\n";
            return error_send;
        } else  {
            cerr<<"asked "<<size<<"sent "<< error <<"too big chunk\n";
            return error_send;
        }
    }

    cout<<"sent data\n";

    int nread = recv(socket,data_out,MAX_SIZE_BUF,0);
    if (nread == -1 ) {
       cerr<<"error read\n";
       return error_accept;
    }
    if ( nread == 0 ) {
        cerr<<"connection was closed on the other side\n";
        return error_closed;
    }

    cout<<"received data\n";
    return nread; // number of gotten bytes
}
*/

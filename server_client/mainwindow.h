#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QTime>
#include <QTimer>
#include <QMessageBox>
#include "network.h"

#include "dialog_password.h"
#include "inclusions.h"


namespace Ui {
class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_pushButton_pressed();
    void on_plainTextEdit_textChanged();
    void connection_checker();


    void button_authorisation_clicked();


    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

#define CONNECTED_VALUE 1
#define DISCONNECTED_VALUE 2


private:
    Ui::MainWindow *ui;
    int connect_disconnect_counter;
    Client_network *_network;
    QTimer *_timer;

};

#endif // MAINWINDOW_H

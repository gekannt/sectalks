#ifndef NETWORK_H
#define NETWORK_H

#include "inclusions.h"
#include "../server_server/data.h"
#include "store_data.h"

//#define ADDRESS "198.252.206.16"
#define ADDRESS "127.0.0.1"
#define PORT "49000"


const int error_send = -1, error_accept = -2, error_closed = -3;
static char gl_data[MAX_SIZE_BUF];



// if number accepted bytes is
struct hold {
    int size;
    char *p_to_data;
};


// list <hold>
class Client_network
{

  string secret_hash;

 public:
//    static Authenticate _auth_static_field;

    Client_network();
    int Ninitialise();
    int Nsend(const char *data, int size);
    int Nsend(const string &data);
    char *Nreceive(int socket);
    int Ndata_exchange();
    int CN_handshake();

    int is_server_was_found();
    int initial_try_to_connect();
    int send_test_presense_data();

    void set_connected_state();
    void set_disconnected_state();
    void disconnect();
    int is_connected() const;

    Authenticate get_name_passw_from_store();
    Authenticate get_name_passw_from_class();

 private:


    int _socket_fd;
    int _server_is_present;
};

#endif // NETWORK_H

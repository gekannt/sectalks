#ifndef INCLUSIONS_H
#define INCLUSIONS_H

#include <iostream>
using namespace std;

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>

#define CHECKING_INTERVAL_SERVER_PRESENCE 1000

#define MAX_SIZE_BUF 100000

#define MAX_LENGTH 2000
#define DEFAULT_FILE_STORE "/home/sh/progr.txt"

#define REPORT_EXIT(s)  { \
    printf("%s  %d  %s",__FILE__,__LINE__,s); \
    exit(EXIT_FAILURE); \
}

#define REPORT_CONTINUE(s) {  \
    fprintf(stderr,"%s  %d  %s",__FILE__,__LINE__,s); \
    exit(EXIT_FAILURE); \
}

#endif // INCLUSIONS_H

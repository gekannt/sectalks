#include "word.h"

Word::Word() {
    msearch=false;
    mholder_replace.clear();
    mnumber_word_from_mholder_replace=0;
}

bool Word::operator ==(const string &line) {
    if ( this->word == line )
      return false;
  return true;
}

Word Word::operator =(const Word &source)  {
    part_of_speech.clear();
    word.clear();
    generation_part_of_speech.clear();

     this->word.assign(source.word.begin(),source.word.end());
     this->part_of_speech.assign(source.part_of_speech.begin(),source.part_of_speech.end());
     this->msearch=source.msearch;
     this->generation_part_of_speech=source.generation_part_of_speech;
    return *this;
}

Word Word::operator=(string source)  {
         this->word.assign(source.begin(),source.end());
        return *this;
}


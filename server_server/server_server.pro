TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    network.cpp \
    ../server_client/protocol.cpp \
    database.cpp \
    collection.cpp \
    sentence_task.cpp \
    word.cpp \
    replacement.cpp \
    data.cpp

HEADERS += \
    network.h \
    inclusions.h \
    ../server_client/protocol.h \
    database.h \
    collection.h \
    sentence_task.h \
    word.h \
    replacement.h \
    data.h

QMAKE_CXXFLAGS += "-std=c++11"
CXXFLAGS="-std=c++0x"

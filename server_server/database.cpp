#include "database.h"

Database::Database(const char *namefile) : _collection(namefile)
{

//    info_about_initialisation.clear();
//    _database.open(namefile,ios_base::in);
//    if (_database.is_open() == false)
//        info_about_initialisation = "can't open database";
}


int Database::check_presense(const string &input) const {

    for(auto iter = _container.cbegin(); iter!= _container.cend();
        ++iter)
        if ( iter->_name == input)
            return true;

    return false;
}

int Database::validate_password(const Authenticate &input) const {
    for(auto iter= _container.cbegin(); iter!= _container.cend();
        ++iter)
        if ( *iter == input)
            return true;

  return false;
}

string Database::get_line() {
   return _collection.get_line();
}

char Database::no_more_lines() {
  return  _collection.no_more_lines();
}

int Database::read_database() {
 _collection.read_full_file();

 Authenticate field;

 while ( this->no_more_lines() == false) {
    string line =this->get_line();

    Sentence sentence(line);
    sentence.parse_sentence();

    Word line2;

    string *ptr = &field._name;
//    string *hold = ptr;
    int counter_words_in_sentence = 0;
  while ( (line2 = sentence.get_one_word()) == string("")
        && counter_words_in_sentence < MAX_VALUE_WORDS_IN_SENTENCE+1  )  {
    *ptr++ = line2.word;
     counter_words_in_sentence++;

  }

  if ( counter_words_in_sentence >  Database::MAX_VALUE_WORDS_IN_SENTENCE) {
      cerr<<line<<endl;
      REPORT_EXIT("integrity of database is broken, too many words in one sentence");
  }


//  cout << field._name<<"  "<<field._password<<"\n";
  _container.push_back(field);
//    cout<<"1 "<<*hold++<<"\n";
//    cout<<"2 "<<*hold<<"\n";

 }

}


void Database::show() const {
//  _collection.show();

//    // 1
//    for (int i=0; i<_container.size(); i++)
//        cout<< _container[i]._name<<"  "<<  _container[i]._password<<"\n";

////// 2
//    for( auto iter = _container.cbegin(); iter != _container.cend(); iter++) {
//        cout <<           iter->_name                <<"\n";
//    }

  //3
    for( auto &x : _container)
        cout<< x._name<< "  " << x._password<<"\n";
}

int Database::get_number_of_lines() const {
    return _collection.get_number_of_lines();
}


#ifndef NETWORK_H
#define NETWORK_H

#include "inclusions.h"
#include  "../server_client/protocol.h"
#include "database.h"

#define MAX_SIZE_BUF 100000
#define NUMBER_INCOME 10


class Accept {
  public:

  private:
   int socket_descriptor;
};

class Server_network
{
public:
    static char gl_data[MAX_SIZE_BUF];

    Server_network(const char *port, Database &database);
    int SN_initialise();
    int SN_listen();
    int SN_accept();
    int SN_handshake();
    int SN_send();

private:
    struct addrinfo hints;
    struct addrinfo *result, *result_pointer;
    int sfd, error;
    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len;
    ssize_t nread;
    socklen_t addr_size;
    int new_fd;

    Database &_database;
    const char *_port;

};

#endif // NETWORK_H

#ifndef DATABASE_H
#define DATABASE_H

#include "inclusions.h"
#include "collection.h"
#include "sentence_task.h"
#include "data.h"


#define DATABASE_NAMEFILE "database.txt"

class Database
{
 public:
  explicit Database(const char *namefile);
  int read_database();
  void show() const;
  int get_number_of_lines() const;
  int check_presense(const string &input) const;
  int validate_password(const Authenticate &input) const;


  string get_line();
 private:
   char no_more_lines();

   Collection _collection;
   ifstream _database;
   string info_about_initialisation;
   vector <Authenticate> _container;

   const int MAX_VALUE_WORDS_IN_SENTENCE = 2;
};

#endif // DATABASE_H


//Database database (DATABASE_NAMEFILE);
//database.read_database();


//if ( database.check_presense("alan") )
//    cout<<"is present"<<endl;

//if( database.validate_password( Data("alan","passgg")  ))
//    cout<<"validated password";

////   cout<<database.get_number_of_lines()<<"\n";


//database.show();


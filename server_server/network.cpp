#include "network.h"

Server_network::Server_network(const char *port, Database &database) : _port(port), _database(database)
{
    memset(&hints,0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; // fill in my IP for me
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    addr_size = sizeof( sockaddr_storage );
}
// SN   - server network
int Server_network::SN_initialise() {

    error = getaddrinfo(NULL,_port,&hints,&result);


    if ( error != 0) {
        cerr<<__FILE__<<" "<<__LINE__<<" error getaddrinfo"<<gai_strerror(error)<<"\n";
        return -1;
    }

    for (result_pointer = result;
         result_pointer != NULL; result_pointer = result->ai_next) {

        sfd = socket(result_pointer->ai_family,
                     result_pointer->ai_socktype,
                     result_pointer->ai_protocol);

        if ( sfd == -1)
            continue;
        if(bind ( sfd,result_pointer->ai_addr,result_pointer->ai_addrlen)==0 )
            break; // successful

        close(sfd);
    }

    if ( result_pointer == NULL) {
        cerr<<"can't bind\n";
        return -2;
    }

    freeaddrinfo(result);
}

int Server_network::SN_listen() {
    cout<<"listen waiting\n";

    if ( listen(sfd,NUMBER_INCOME) != 0) {
        cerr<<"listen failed\n";
        return -1;
    }
}

int Server_network::SN_handshake() {

}

int Server_network::SN_accept() {

// set up and hold on connection
  for(; ; )  {
    cout<<"\n";
    new_fd = accept(sfd,(struct sockaddr *)&peer_addr,
                    &addr_size);
//    int  counter = 0;

    pid_t pid = fork();
    if (pid != 0) // parent
        continue;

   this->SN_handshake();


   for( ; ;) {
    cout<<"accept inside handling\n";

    if ( new_fd  == -1) {
        cerr<<gai_strerror(errno)<<"can't accept\n";
        exit(EXIT_FAILURE);
    }

     char buf[2000]={0, };
     int error =  recv(new_fd,buf,2000,0);
     cout<<buf<<"\n";

     static int a = 0;

     if ( a == 0) { // initialisation
         cout<<"first\n";
         string b(buf);
         Authenticate out;
         Protocol::basic_unpack(b,out,AUTHORISATION);
         if (  _database.validate_password(out) != true ) { // password wasn't validated
             cout<<"wrong password, closing connection\n";

             string out;
             const char *result_operation_l = "failed";
             if ( Protocol::usual_pack(result_operation_l,out) == false)
                 REPORT_CONTINUE("error usual packing 1");

             send(new_fd,out.c_str(),out.length(),0);

             shutdown(new_fd,SHUT_RDWR);
//             close(new_fd);
             break;
//             exit(0);
         }
         else { // send back response about success
             cout<<"validation passed successful\n";
             const char *result_operation_l = "success";
//             string result_operation = "success";
             string out;
             if ( Protocol::usual_pack(result_operation_l,out) == false )
                 REPORT_CONTINUE("error usual packing 2");

             send(new_fd,out.c_str(),out.length(),0);
         }

         cout<<"depacked  "<<out._name <<"  "<< out._password<<"\n";
         a++;
     }



     if (error == 0) {
         cout<<"client closed connection\n";
         close(new_fd);
         break;
//         return 0;  // finish program
     }


//     cout<<"received  "<<error<<"\n";

     if ( error ==-1) {
         cerr<<gai_strerror(errno)<<"recv problem\n";
         exit(EXIT_FAILURE);
     }
    }

  }

//  wait();
}


//void get_ip_address() {
//    struct sockaddr addr;
//    socklen_t length= sizeof(struct sockaddr);

//    if ( getpeername(new_fd,&addr,&length) == 0 ) {
//        cout<<"successs";
//    }

//}

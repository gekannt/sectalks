#ifndef INCLUSIONS_H
#define INCLUSIONS_H

#include <sys/types.h>
#include <unistd.h>
#include <cstring>
#include <sys/socket.h>
#include <netdb.h>


#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <errno.h>

#include <vector>
#include <fstream>
#include <set>
#include <algorithm>
using namespace std;


#define REPORT_EXIT(s)  { \
    printf("%s  %d  %s",__FILE__,__LINE__,s); \
    exit(EXIT_FAILURE); \
}

#ifdef WIN32_
 #include  <WinSock2.h>
 #include <WS2tcpip.h>
 #pragma  comment (lib,"Ws2_32.lib")
#else

#endif


#define REPORT_EXIT(s)  { \
    printf("%s  %d  %s",__FILE__,__LINE__,s); \
    exit(EXIT_FAILURE); \
}

#define REPORT_CONTINUE(s) {  \
    fprintf(stderr,"%s  %d  %s",__FILE__,__LINE__,s); \
    exit(EXIT_FAILURE); \
}



#endif // INCLUSIONS_H

#ifndef DATA_H
#define DATA_H

#include "inclusions.h"

struct Authenticate {
    Authenticate(const string &name, const string &password):
        _name(name), _password(password) {}
    Authenticate() {}


    int operator == (const Authenticate &input) const {
        if (this->_name == input._name &&
            this->_password == input._password)
            return true;
       return false;
    }

    int is_empty() {
        if ( _name.empty() || _password.empty() )
            return true;
    }

    void show() {
        cout<<this->_name<<"\n"<<this->_password<<"\n";
    }

    string _name,_password;
};
#endif // DATA_H

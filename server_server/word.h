#ifndef WORD_H
#define WORD_H

#include "inclusions.h"

class Word {

 public:
  Word();
  Word operator=(const Word &source);
  Word operator=( string source);
  bool operator==(const string &line);



  friend ostream &operator<<(ostream &stream, Word &source)  {
      stream<<source.word<<"  "<<source.part_of_speech<<"  "<<source.msearch<<"  "<< source.generation_part_of_speech<<"\n";
  }


//  Word operator&=( Word &source)
//  { /* Word for_return_word;
//      for_return_word.word.assign(source.word.begin(),source.word.end());
//      for_return_word.part_of_speech.assign(source.part_of_speech.begin(),source.part_of_speech.end());
//     return for_return_word;*/
//          part_of_speech.clear();
//          word.clear();

//           this->word.assign(source.word.begin(),source.word.end());
//           this->part_of_speech.assign(source.part_of_speech.begin(),source.part_of_speech.end());
//          return *this;
//  }
//  private:
    string part_of_speech,word,generation_part_of_speech;
    bool msearch;
    set<string> mholder_replace;
    size_t mnumber_word_from_mholder_replace;

};


#endif // WORD_H

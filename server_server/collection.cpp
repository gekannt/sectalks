#include "collection.h"

Collection::Collection(const char *input_file) {

      minput_data.open(input_file,ios_base::in);

      if ( ! minput_data.is_open())  REPORT_EXIT("not opened");

      mstorage = new(nothrow) char[mstandard_length_of_sentence]();

      if ( mstorage == NULL)  REPORT_EXIT("memory cannot be taken, new operator returned NULL");

      increase_size = 1; // for reallocation of memory if it's not enough of it

   mno_more_lines = false;
   mcount_of_given_lines = 0;
}

void Collection::reset_to_first_line() {
    mcurrent_line=mcontainer.begin();
}


char  Collection::no_more_lines() {
  return mno_more_lines;
}

size_t Collection::get_number_of_lines() const {
 return mcontainer.size();
}


// iternal class function
int Collection::read_line() {
    minput_data.clear(); // clear all previous possible mistakes
    minput_data.getline(mstorage+strlen(mstorage),mstandard_length_of_sentence*increase_size-strlen(mstorage));

    if (minput_data.eof())  return EOF; //end of file has been reached

    if(minput_data.fail() ) { // not enough memory to hold the whole line
        char *temp_p = mstorage;

        const size_t INCREASE_SIZE_OF_BUFFER=2;
        increase_size *= INCREASE_SIZE_OF_BUFFER;

       #ifdef DEBUG_SOURCE_SENTENCES
        cout<<increase_size*mstandard_length_of_sentence<<"\n";
       #endif

        mstorage = new(nothrow) char[mstandard_length_of_sentence*increase_size]();
        if ( mstorage == NULL) REPORT_EXIT("memory cannot be reallocated");

        memcpy(mstorage,temp_p,strlen(temp_p));

      delete []temp_p;
      this->read_line();

      return 0;
    }

   #ifdef DEBUG_SOURCE_SENTENCES
    cout<<mstorage<<endl;
   #endif
   return 0;
}

string Collection::get_line() {
    mcount_of_given_lines++;

    if ( mcount_of_given_lines == this->get_number_of_lines())
      mno_more_lines = true;

    return *mcurrent_line++;
}

void Collection::read_full_file() {

    int result_of_last_reading= 2;
    while(  result_of_last_reading != EOF )  {

        result_of_last_reading=this->read_line();

      string line=mstorage;
     if (!line.empty())  { // don't add empty line`
        mcontainer.push_back(line);
        mstorage[0] = '\0';
     }
    }

    this->reset_to_first_line();
}

void Collection::show() {
    for(int i=0; i<mcontainer.size(); i++)
        cout<<mcontainer[i]<<"\n";
    cout<<flush;
}

Collection::~Collection() {
   minput_data.close();
   delete []mstorage;
}

#ifndef PARSING_SENTENCE_H
#define PARSING_SENTENCE_H

#include "inclusions.h"
#include "word.h"
#include "replacement.h"


class Sentence {
#define SENTENCE_RESERVED_DIVISOR '@'

 public:
    Sentence();
    Sentence(string line);
    ~Sentence();

    void show_together();
    void show_separately();
    void show_generated();
    void show_current();

    int parse_sentence();
    string form_sentence();
    Word get_one_word();


  protected:
      vector<string>::iterator mcurrent_line;
      vector<string>::iterator mfirst_line_iterator;
      size_t mnumber_of_lines, mnumber_of_words;
      Word *mstorage_words;

//      vector<Word> _words;
      char mend_of_sentences;
      string mline_input;

      size_t number_of_words_in_sentence();
      Word parse_word(string &word);

 private:
   int _counter_sentence;
   friend class Replacement;
   int set_question_mark; // replace to char ! and get core dump
   static int number_of_processed_lines_;


};

#endif // PARSING_SENTENCE_H

#ifndef REPLACEMENT_H
#define REPLACEMENT_H

#include "sentence_task.h"
#include "collection.h"
#include "inclusions.h"

class Sentence;


class Replacement {
 public:
    static vector<string> mgenerated_strings;
    Replacement(Sentence &sentence);

    void generate_words_from_wn();
    void generate_permutations();
    int show_invitation_edit();

    friend ostream &operator<<(ostream &stream, Replacement &replacement){
        for(static size_t i=0;i<mgenerated_strings.size(); i++)
            stream<<replacement.mgenerated_strings[i];
    }

    static const  char kREPLACEMENT_TEMP_FILE_[], kREPLACEMENT_VERB_[],kREPLACEMENT_NOUN_[],
                       kREPLACEMENT_ADJECTIVE_[], kREPLACEMENT_ADVERB_[];

//    static const int fg=4;
//    static const float gfgh = 5.4;

 private:
  Sentence *msentence;
  void check_correctness_of_requested_transformation();
  string form_command(size_t i);
};

#endif // REPLACEMENT_H
